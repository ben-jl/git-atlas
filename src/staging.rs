use std::path::Path;

use git2::{Cred, FetchOptions, RemoteCallbacks, Repository};
use log::info;
use uuid::Uuid;

pub fn from_url(url: &str) -> anyhow::Result<Repository> {
    let repository = execute_stage_command(url)?;

    Ok(repository)
}

fn execute_stage_command(url: &str) -> anyhow::Result<Repository> {
    info!("Staging repository {}", &url);
    let staging_path = format!("/tmp/git-atlas/{}", Uuid::new_v4());
    let p = Path::new(&staging_path);

    let mut cb = RemoteCallbacks::new();
    cb.credentials(|_url, _u, _at| Cred::default());
    cb.transfer_progress(|p| {
        let sofar = p.received_objects();
        let total = p.total_objects();
        if sofar % 250 == 0 {
            info!("{} {}", sofar, total);
        }
        true
    });

    let mut fetch_options = FetchOptions::new();
    fetch_options.remote_callbacks(cb);

    let mut builder = git2::build::RepoBuilder::new();
    builder.fetch_options(fetch_options);
    let r = builder
        .clone(url, p)
        .expect("Error cloning git repository to staging location");
    info!("Successfully staged repository {} to {}", &url, p.display());
    Ok(r)
}
