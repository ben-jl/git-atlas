use log::trace;

use crate::facade::RepoFacade;
pub struct GitAtlasProcess {
    repo_facade: RepoFacade,
}

impl GitAtlasProcess {
    pub fn new(repo_facade: RepoFacade) -> GitAtlasProcess {
        GitAtlasProcess { repo_facade }
    }

    pub fn render(&self) -> anyhow::Result<()> {
        trace!(
            "Starting render with HEAD set to {}",
            &self.repo_facade.location().display()
        );
        trace!("Render complete");
        Ok(())
    }
}
