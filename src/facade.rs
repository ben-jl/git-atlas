use git2::{Commit, Oid, Repository};
use log::trace;
use std::fmt::Display;
use std::path::Path;

pub struct RepoFacade {
    repo: Repository,
}

impl RepoFacade {
    pub fn new(repo: Repository) -> RepoFacade {
        RepoFacade { repo }
    }

    pub fn location(&self) -> &Path {
        &self.repo.path()
    }

    pub fn head(&self) -> anyhow::Result<CommitNode> {
        let commitref = self.repo.head()?;
        let commit = commitref.peel_to_commit()?;
        let cn = commit.into();
        Ok(cn)
    }

    pub fn parents(&self) -> anyhow::Result<Vec<CommitNode>> {
        let h = self.head()?;
        let oid = Oid::from_str(h.sha())?;

        let c = self.repo.find_commit(oid)?;
        let ps = c
            .parent_ids()
            .into_iter()
            .filter_map(|e| self.resolve(e).ok())
            .collect::<Vec<CommitNode>>();
        Ok(ps)
    }

    pub fn parents_of(&self, commit: &CommitNode) -> anyhow::Result<Vec<CommitNode>> {
        let oid = Oid::from_str(commit.sha())?;
        let c = self.repo.find_commit(oid)?;
        let ps = c
            .parent_ids()
            .into_iter()
            .filter_map(|e| self.resolve(e).ok())
            .collect::<Vec<CommitNode>>();
        Ok(ps)
    }

    pub fn walk_from_current(&self) -> anyhow::Result<CommitNodeIterator> {
        let repo = self;
        let queued = self.parents()?;
        Ok(CommitNodeIterator { repo, queued })
    }

    pub fn references(&self) -> anyhow::Result<Vec<Ref>> {
        let rs = self.repo.references()?;
        let mut refvec: Vec<Ref> = Vec::new();
        for r in rs {
            let rf = r?;
            trace!("{}", rf.name().unwrap_or("NA"));
            let c = rf.peel_to_commit()?;
            trace!("Peeled to commit {}", c.summary().unwrap_or("NA"));
            let cn: CommitNode = c.into();
            let name: String = rf.name().unwrap_or("NA").into();
            let refn = Ref {
                commit: cn,
                name: name,
            };
            refvec.push(refn);
        }
        Ok(refvec)
    }

    fn resolve(&self, oid: Oid) -> anyhow::Result<CommitNode> {
        let c = self.repo.find_commit(oid)?;
        let cn = c.into();
        Ok(cn)
    }
}

pub struct CommitNodeIterator<'a> {
    repo: &'a RepoFacade,
    queued: Vec<CommitNode>,
}

impl<'a> Iterator for CommitNodeIterator<'a> {
    type Item = CommitNode;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(cn) = self.queued.pop() {
            let ps_of = self.repo.parents_of(&cn.clone()).ok()?;
            self.queued.extend(ps_of);
            Some(cn.clone())
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct CommitNode {
    sha: String,
    message: String,
    parents: Vec<Oid>,
}

impl CommitNode {
    fn sha(&self) -> &str {
        &self.sha
    }
}

impl<'a> From<Commit<'a>> for CommitNode {
    fn from(c: Commit<'a>) -> Self {
        CommitNode {
            sha: format!("{}", c.id()),
            message: c.message().unwrap_or("").into(),
            parents: c.parent_ids().collect(),
        }
    }
}

impl Display for CommitNode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let ps: String = self
            .parents
            .iter()
            .fold("".into(), |curr, nxt| format!("{} {}", curr, nxt));
        write!(f, "{} {}", &self.sha, &ps)
    }
}

pub struct Ref {
    commit: CommitNode,
    name: String,
}

impl Display for Ref {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.name, &self.commit)
    }
}
