#![allow(dead_code)]
mod facade;
mod process;
mod staging;

use clap::{App, Arg, SubCommand};
use log::{info, warn, LevelFilter};
use simplelog::{ColorChoice, Config, TermLogger, TerminalMode};

fn main() -> anyhow::Result<()> {
    let ms = App::new("git-atlas")
        .about("Terminal app for visualizing and planning complex git operations")
        .version("0.0.1")
        .author("Ben LeValley <ben.levalley@gmail.com>")
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .help("Sets the log level (-v for info, -vv for debug, -vvv for trace)")
                .required(false)
                .multiple(true)
                .takes_value(false),
        )
        .subcommand(
            SubCommand::with_name("stage")
                .about("Clone a repository and open it in staging view")
                .arg(
                    Arg::with_name("url")
                        .help("Url of the repository to stage")
                        .required(true),
                ),
        )
        .get_matches();

    let verbosity_count: u8 = ms
        .occurrences_of("verbosity")
        .try_into()
        .expect("Verbosity flag must occur between 0 and 4 times");
    initialize_logging(verbosity_count)?;

    if let Some("stage") = ms.subcommand_name() {
        let url = ms
            .subcommand_matches("stage")
            .unwrap()
            .value_of("url")
            .expect("Must include url in stage command");
        let repo = staging::from_url(&url)?;
        let facade = facade::RepoFacade::new(repo);

        for r in facade.references().unwrap() {
            info!("{}", r);
        }
        let atlas = process::GitAtlasProcess::new(facade);
        atlas.render()?;
    }

    Ok(())
}

fn initialize_logging(level_index: u8) -> anyhow::Result<()> {
    let lf = match level_index {
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        3 => LevelFilter::Trace,
        _ => LevelFilter::Warn,
    };

    TermLogger::init(
        lf,
        Config::default(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    )
    .expect("Unable to configure logger");

    if level_index > 3 {
        warn!("Received unexpected level filter count, using max value of 3 instead of received count of {}", level_index);
    }
    Ok(())
}
