# git-atlas

A terminal app to visualize and plan complex operations on a git repository

## Troubleshooting

### Compilation Failures

#### Error: failed to run custom build command for `openssl-sys v0.9.67`

1. Ensure openssl libs installed (if on Linux)

    ```bash
    sudo apt-get install libssl-dev
    ```

2. Ensure pkg-config installed (if on Linux)

    ```bash
    sudo apt-get install pkg-config
    ```
